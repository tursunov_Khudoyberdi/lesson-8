(function () {
  let minutes = 0,
    seconds = 0,
    tens = 0,
    aMin = document.getElementById("minutes"),
    aSec = document.getElementById("seconds"),
    startBtn = document.getElementById("start"),
    pauseBtn = document.getElementById("pause"),
    addBtn = document.getElementById("add"),
    resetBtn = document.getElementById("reset"),
    deleteBtn = document.getElementById("delete"),
    addedS = document.getElementById("addedS"),
    interval,
    timeAdded = { mins: 0, seconds: 0, tens: 0 };

  function leftPad() {
    return value < 10 ? ("0" + value).slice(-2) : value;
  }

  function supportsLocalStorage() {
    return typeof Storage !== "undefined";
  }

  pauseBtn.style.visibility = "hidden";
  addBtn.style.visibility = "hidden";

  startBtn.onclick = function () {
    clearInterval(interval);
    interval = setInterval(timer, 10);
    pauseBtn.classList.toggle();
    startBtn.classList.toggle();
    addBtn.classList.toggle();
    resetBtn.classList.toggle();
  };

  pauseBtn.onclick = function () {
    clearInterval(interval);
    startBtn.classList.toggle();
    pauseBtn.classList.toggle();
    addBtn.classList.toggle();
    resetBtn.classList.toggle();
  };

  resetBtn.onclick = function () {
    clearInterval(interval);
    minutes = "00";
    seconds = "00";
    aMin.innerHTML = minutes;
    aSec.innerHTML = seconds;
  };

  function compare(a, b) {
    let ret = {};
    for (let key in a) {
      let time1 = a[key];
      let time2 = b[key];
      ret[key] = (-time2 + time1).slice(-2);
    }
    let addedMin = ret.minutes,
      addedSec = ret.seconds;
    return ret;
  }

  function timer() {
    tens++;
    if (tens > 99) {
      seconds++;
      aSec.innerHTML = "0" + seconds;
      tens = 0;
    }
    if (seconds > 9) {
      aSec.innerHTML = seconds;
    }
    if (seconds > 59) {
      minutes++;
      aMin.innerHTML = "0" + minutes;
      seconds = 0;
      aSec.innerHTML = "0" + 0;
      tens = 0;
    }
    if (minutes > 9) {
      aMin.innerHTML = minutes;
    }
    timeAdded = {
      tens: ("0" + tens).slice(-2),
      seconds: ("0" + seconds).slice(-2),
      minutes: ("0" + minutes).slice(-2),
    };

    // Current lap

    if (supportsLocalStorage()) {
      localStorage.setItem("timer", timeAdded);
    }

    if (!supportsLocalStorage()) {
      console.log("It is not cool.");
    } else {
      try {
        addBtn.onclick = function () {
          let addedMinutes = minutes - timeAdded.mins;
          if (addedMinutes < 0) {
            addedMinutes = minutes - timeAdded.mins + 60;
          }
          let addedSeconds = seconds - timeAdded.seconds;
          if (addedSeconds < 0) {
            addedSeconds = seconds - timeAdded.seconds + 60;
          }
          let addedTens = tens - timeAdded.tens;
          if (addedTens < 0) {
            addedTens = tens - timeAdded.tens + 100;
          }
          timeAdded = {
            tens: ("0" + addedTens).slice(-2),
            seconds: ("0" + addedSeconds).slice(-2),
            mins: ("0" + addedMinutes).slice(-2),
          };
          document.getElementById("addedMin").innerHTML +=
            "<li>" +
            "Added " +
            addedCount++ +
            " - " +
            leftPad(addedMinutes) +
            ":" +
            leftPad(addedSeconds) +
            "</li>";

          localStorage.setItem("addedCounts", addedCount);
          localStorage.setItem("addeds", addedS.innerHTML);
        };
      } catch (error) {
        return error.message;
      }

      if (localStorage.getItem("addeds")) {
        let storedAddeds = localStorage.getItem("addeds");
        addedS.textContent = storedAddeds;
      }

      if (localStorage.getItem("addedCounts")) {
        addedCount = localStorage.getItem("addedCounts");
      }
      deleteBtn.onclick = function () {
        addedS.innerHTML = "";
        localStorage.removeItem("addeds");
        addedCount = 1;
        localStorage.removeItem("addedCounts");
      };

      if (localStorage.getItem("timer")) {
        let storedTimer = localStorage.getItem("timer");
        aMin.innerHTML = storedTimer.minutes;
        aSec.innerHTML = storedTimer.seconds;
        minutes = storedTimer.minutes;
        seconds = storedTimer.seconds;
        tens = storedTimer.tens;

        timeAdded = {
          tens: storedTimer.tens,
          seconds: storedTimer.seconds,
          minutes: storedTimer.minutes,
        };
      }
    }
  }
})();
